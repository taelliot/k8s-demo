# Hello World Deployment Demo

Below are the commands that were run in the demo today along with more information.

# v1 Basics
This will make a simple pod that no one can really use.
```
# Create the pod
kubectl create -f v1
# Cleanup
kubectl delete -f v1
```

# v2 More Useful
This will make the same pod, but expose a [ClusterIP](http://kubernetes.io/docs/user-guide/services/#publishing-services---service-types) service that allows other pods to use it. There is no way to access these resources from outside of the cluster...yet!

*Create the service and pod*
```
kubectl create -f v2
# look at the service info
kubectl describe svc hello-world-service
```

*Check the ns lookup for our service*
```
kubectl run -it curlpod --image=radial/busyboxplus:curl --namespace=demo

# hit enter to go into the container
nslookup hello-world-service

# let's hit it
curl hello-world-service
```

You can reference `hello-world-service` from anywhere in your namespace and it will resolve to that pod.

```
# Cleanup
kubectl delete -f v2
```

# v3 Getting More Serious

Make it so we can talk to our pod from the outside world.

```
kubectl create -f v3
watch kubctl describe svc hello-world-service
# wait for an "external-ip" to show up
# go to aws and watch for your elb health checks 
# to pass

# Cleanup
kubectl delete -f v3
```

# v4 Real World
We saw that we only have 1 of our 2 instances in service - we need more pods!

```
kubectl create -f v4
# do the same song and dance as above
# now let's scale
kubectl scale rc hello-world-rc --replicas=10

# Cleanup
kubectl delete -f v4
```

# v5 More Glitz
Let's add a route53 entry. This uses a [plugin for Kubernetes](https://github.com/wearemolecule/route53-kubernetes) that reads annotation data and performs a job.

```
kubectl create -f v5

# Cleanup
kubectl delete -f v3
```

We can hit this online [here](http://demo.devalertlist.com)!

# Extras

## Load Testing
Running the load tests - pull down [hello world](https://drone.devalertlist.com/taelliot/hello-world) and go into the project directory.
```
git clone git@bitbucket.org:taelliot/hello-world.git && cd hello-world
```

Go into the projects directory and run the following:

```
docker run -p 1337:8089 --rm -v "$(pwd)"/tests/load:/tests docker.devalertlist.com/locustio -f /tests/locustfile.py --host=http://demo.devalertlist.com/
```
Open [this](http://localhost:1337) and add some locusts to your swarm and start them up.

## You can try applying instead of deleting and creating
Instead of ```kubectl delete -f``` you can try to ```kubectl apply -f``` above and update the environment.

# Slides
You can get the slides [here](https://bitbucket.org/taelliot/k8s-demo/raw/master/Kubernetes_101.pptx)
